<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/**********/
/** Admin */
/**********/


Route::get('/admin', 'Admin\ArticleController@index');
Route::resource('tasks', 'Admin\TasksController');
Route::get('/tasks-success', 'Admin\TasksController@index')->name('taskSuccess');

Route::get('/delete-task/{id}', 'Admin\TasksController@destroy');
Route::post('/tasks/{id}/update', 'Admin\TasksController@update');

Route::get('/lessons/{week}', 'Admin\ArticleController@index');

Route::get('/admin/students', 'Admin\ArticleController@index')->name('students');
Route::get('/admin/parents', 'Admin\ArticleController@index')->name('parents');
Route::get('/admin/teachers', 'Admin\ArticleController@index')->name('teachers');

Route::get('/create-new-entry', 'Admin\ArticleController@create')->name('create-new-entry');
Route::get('/blog-entries', 'Admin\ArticleController@index')->name('blog-entries');
Route::post('/store-article', 'Admin\ArticleController@store')->name('store-article');
Route::post('/update-article/{id}', 'Admin\ArticleController@update');
Route::get('/article-edit/{id}', 'Admin\ArticleController@edit');

Route::get('/create-new-category', 'Admin\CategoryController@create')->name('create-new-category');
Route::get('/category-entries', 'Admin\CategoryController@index')->name('category-entries');

Route::post('/store-category', 'Admin\CategoryController@store')->name('store-category');
Route::get('/category-edit/{id}', 'Admin\CategoryController@edit');
Route::post('/update-category/{id}', 'Admin\CategoryController@update');

Route::get('/galleries', 'Admin\GalleryController@index')->name('galleries');
Route::get('/create-galleries', 'Admin\GalleryController@create')->name('create-gallery');
Route::post('/store-gallery', 'Admin\GalleryController@store');
Route::get('/gallery-delete/{id}', 'Admin\GalleryController@delete');
Route::get('/gallery-edit/{id}', 'Admin\GalleryController@edit');
Route::post('/update-gallery/{id}', 'Admin\GalleryController@update');

/** File upload */
Route::get('/uploadfile', 'Admin\UploadFileController@index');
Route::post('/uploadfile', 'Admin\UploadFileController@store');
Route::get('/delete-image/{gallery_id}/{pic_id}', 'Admin\UploadFileController@delete');


/** Front End*/
Route::get('/', 'Frontend\HomeController@index')->name('home');
Route::get('/home', 'Frontend\HomeController@index')->name('home');
Route::get('/homepage', 'Frontend\HomeController@homepage');
Route::get('/blog', 'Frontend\HomeController@index')->name('home');

Route::get('/category/{id}', 'Frontend\HomeController@category');

Route::get('/article/{id}', 'Frontend\HomeController@article');
Route::get('tasks/show/{id}', 'Frontend\TasksController@show');
Route::get('/gallery/album/{id}', 'Frontend\AlbumController@index');
Route::get('/events', 'Frontend\EventsController@index');

/**********/
/** AJAX */
/**********/

Route::get('/increase-likes/{id}', 'Frontend\HomeController@likeIt');
