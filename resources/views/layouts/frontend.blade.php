<!DOCTYPE html>
<html>
<head>
    <title>Újlaki Általános Iskola</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="Primary School">

    <link href="/css/bootstrap.css" rel="stylesheet">
    <!-- Custom Theme files -->

    <script src="/js/jquery.js"></script>
    <!--webfonts-->

    <link href="/css/css.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <!--//webfonts-->

</head>
<body>


@yield('content')


<!--//address-->

<!----footer--->
<div class="footer">
    <div class="container">
        <div class="copy">
            <p>Copyright © 2019 dogisoft.hu | All rights reserved | Design by <a href="http://w3layouts.com/">dogisoft</a>
            </p>
        </div>

    </div>
</div>

<a href="#home" id="toTop" class="scroll" style="display: block;">
    <span id="toTopHover" style="opacity: 1;"> </span></a>
</body>
</html>
