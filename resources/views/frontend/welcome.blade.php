@extends('layouts.frontend')

@section('content')

    <!----- start-header---->

    <div id="home" class="header">
        <div class="top-header">
            <div class="container">
                <div class="logo">
                    <a href="">
                        <img src="images/logo.png">
                    </a>
                </div>
                <div class="top-menu">
                    <span class="menu"> </span>
                    <ul class="cl-effect-16">
                        <li><a class="active" href=""
                               data-hover="Főoldal">Főoldal</a></li>
                        <li><a href=""
                               data-hover="Rólunk">Rólunk</a></li>
                        <li><a href="/tanaraink" data-hover="Tanáraink">Tanáraink</a>
                        </li>
                        <li><a href="/galeria" data-hover="Képgaléria">Képgaléria</a>
                        </li>
                        <li><a href="/blog" data-hover="Blog">Blog</a>
                        </li>
                        <li><a href="/kapcsolat" data-hover="Kapcsolat">Kapcsolat</a>
                        </li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <!-- Slideshow 4 -->
        <div class="container">
            <div id="top" class="callbacks_container row">
                <div class="homepage-left col-lg-6">
                    <img src="images/bg_image.jpg" class="homepage-img">
                </div>
                <div class="homepage-right col-lg-6">
                    <h2>A TUDÁS, HATALOM!</h2>
                    <p>A második B osztály büszke arra a szemléletmódra, mely a tanulás mellett az elfogadásra és az emberi léptékre is koncentrál.</p>
                    <p>Ezen szemlélet kialakításában és átadásában tanárainknak elévülhetetlen érdemei vannak! Köszönjük Nekik!</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>


    <!----start-slide-bottom--->
    <div class="slide-bottom">
        <div class="slide-bottom-grids">
            <div class="container">
                <div class="col-md-6 slide-bottom-grid">
                    <h3>Üdvözlet!</h3>
                    <p>Üdvözöljük megújúlt weboldalunkon. Bízunk benne, hogy mindent könnyen és egyszerűen megtalál majd és örömére szolgálnak az új funkciók is!</p>
                </div>
                <div class="col-md-6 slide-bottom-grid">
                    <h3>Küldetésünk</h3>
                    <p>A második B osztály küldetése, hogy ...</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!---728x90--->

    <!--services-->
    <div class="service-section">
        <div class="col-md-7 service-section-grids">
            <div class="container">
                <div class="serve-head">
                    <h3>Tevékenységeink</h3>
                    <h6>Szolgáltatásaink az Ön gyereke számára</h6>
                </div>
            </div>
            <div class="service-grid">
                <div class="service-section-grid">
                    <div class="icon">
                        <i class="book"> </i>
                    </div>
                    <div class="icon-text">
                        <h4>Könyvtár</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            text ever since the 1500s, when an unknown printer.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="service-section-grid">
                    <div class="icon">
                        <i class="pencil"> </i>
                    </div>
                    <div class="icon-text">
                        <h4>Büfé</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            text ever since the 1500s, when an unknown printer.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="service-section-grid">
                    <div class="icon">
                        <i class="award"> </i>
                    </div>
                    <div class="icon-text">
                        <h4>Napközi</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            text ever since the 1500s, when an unknown printer.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-5 service-text">
            <p></p>
        </div>
        <div class="clearfix"></div>
    </div>
    <!---728x90--->

    <!--/services-->
    <div class="news-section">
        <div class="container">
            <div class="news-head">
                <h3>Aktuális Híreink</h3>
                <p>Kövesse a legfontosabb híreinket</p>
            </div>
            <div class="news">
                @foreach($articles as $article)
                    <div class="col-md-4 test-right01 test1">
                        <img src="/storage/images/articles/thumbnail/{{$article->pics}}" class="img-responsive" alt="">
                        <div class="textbox textbox1">
                            <h4 class="col-md-4 date">{{date('d', strtotime($article->created_at))}}<br> <span>{{date('M', strtotime($article->created_at))}}</span><br>
                                <lable>0 <img src="images/comment.png" class="img-responsive" alt=""></lable>
                            </h4>
                            <p class="col-md-8 news">{{$article->title}}</p>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="culture-section">
        <div class="container">
            <div class="culture-head">
                <h3>Eseményeink</h3>
                <p>Ne hagyd ki az aktuális eseményeinket</p>
            </div>
            <div class="culture">
                <div class="col-md-6 culture-grids">
                    <a href="https://p.w3layouts.com/demos/primary_school/web/single.html"> <img
                                src="images/event1.jpg" class="img-responsive" alt=""></a>
                    <div class="e_date">
                        <h4>15<br> <span>July</span></h4>
                    </div>
                    <a href="https://p.w3layouts.com/demos/primary_school/web/single.html"><h5>ART SESSION</h5></a>
                    <p>Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry.Lorem Ipsum is simply dummy text of the printing
                        and typesetting industry.</p>
                </div>
                <div class="col-md-6 culture-grids">
                    <a href="https://p.w3layouts.com/demos/primary_school/web/single.html"> <img
                                src="images/event2.jpg" class="img-responsive" alt=""></a>
                    <div class="e_date">
                        <h4>15<br> <span>July</span></h4>
                    </div>
                    <a href="https://p.w3layouts.com/demos/primary_school/web/single.html"><h5>ART SESSION</h5></a>
                    <p>Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry.Lorem Ipsum is simply dummy text of the printing
                        and typesetting industry.</p>

                </div>
                <div class="col-md-6 culture-grids">
                    <a href="https://p.w3layouts.com/demos/primary_school/web/single.html"> <img
                                src="images/event3.jpg" class="img-responsive" alt=""></a>
                    <div class="e_date">
                        <h4>15<br> <span>July</span></h4>
                    </div>
                    <a href="https://p.w3layouts.com/demos/primary_school/web/single.html"><h5>ART SESSION</h5></a>
                    <p>Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry.Lorem Ipsum is simply dummy text of the printing
                        and typesetting industry.</p>
                </div>
                <div class="col-md-6 culture-grids">
                    <a href="https://p.w3layouts.com/demos/primary_school/web/single.html"> <img
                                src="images/event4.jpg" class="img-responsive" alt=""></a>
                    <div class="e_date">
                        <h4>15<br> <span>July</span></h4>
                    </div>
                    <a href="https://p.w3layouts.com/demos/primary_school/web/single.html"><h5>ART SESSION</h5></a>
                    <p>Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry.Lorem Ipsum is simply dummy text of the printing
                        and typesetting industry.</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="mid-bg">
        <div class="container">
            <div class="mid-section">
                <h3>Első nap a suliban!</h3>
                <h4>Készen állsz?</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy text and
                    typesetting industry. Lorem Ipsum has been the industry's standard dummy
                    ever since the 1500s,</p>
            </div>
        </div>
    </div>
    <!--address-->
    <div id="contact" class="address">
        <div class="col-md-7 address-left">
            <div class="products">
                <h3>Órarend</h3>
                <ul>
                    <li><a href="#">Testnevelés</a></li>
                    <li><a href="#">Környezet</a></li>
                    <li><a href="#">Matematika</a></li>
                </ul>
            </div>
            <div class="company-adout">
                <h3>Órarend</h3>
                <ul>
                    <li><a href="">About</a></li>
                    <li><a href="">Teacher</a></li>
                    <li><a href="">Contact</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="col-md-5 address-right">
            <h3>Elérhetőség</h3>
            <p>Budapest, Ürömi út</p>
            <p>04 84 25 51 54</p>
            <ul class="bottom">
                <li>Email: info@vackor.hu</li>
                <li>Web: www.vackor.hu</li>
            </ul>

        </div>
        <div class="clearfix"></div>
    </div>

@endsection
