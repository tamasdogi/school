@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


    <div class="container">

        <div class="col-lg-2">
            @include('shared.latest')
        </div>

        <div class="col-lg-8">
            <h3>{{$album}} album</h3>
            <div class="row">
            @foreach($pics as $pic)
                <div class="col-lg-3">
                    <a data-caption="{{$album}}" data-fancybox="gallery" href="/storage/images/thumbnail/{{$pic->title}}">
                        <div class="gallery-item" style="background-image: url('/storage/images/thumbnail/{{$pic->title}}')">

                        </div>
                    </a>
                </div>
            @endforeach
            </div>
        </div>

        <div class="col-lg-2">
            @include('shared.popular')
        </div>


    </div>

@endsection
