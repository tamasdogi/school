@extends('layouts.frontend')

@section('content')


    <div id="home" class="header">
        <div class="top-header">
            <div class="container">
                <div class="logo">
                    <a href="">
                        <img src="/images/logo.png">
                    </a>
                </div>
                <div class="top-menu">
                    <span class="menu"> </span>
                    <ul class="cl-effect-16">
                        <li><a class="active" href=""
                               data-hover="Főoldal">Főoldal</a></li>
                        <li><a href=""
                               data-hover="Rólunk">Rólunk</a></li>
                        <li><a href="/tanaraink" data-hover="Tanáraink">Tanáraink</a>
                        </li>
                        <li><a href="/galeria" data-hover="Képgaléria">Képgaléria</a>
                        </li>
                        <li><a href="/blog" data-hover="Blog">Blog</a>
                        </li>
                        <li><a href="/kapcsolat" data-hover="Kapcsolat">Kapcsolat</a>
                        </li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <!-- Slideshow 4 -->
        <div class="container">
            <div id="top" class="callbacks_container row">
                <div class="homepage-left col-lg-6">
                    <img src="/images/bg_image.jpg" class="homepage-img">
                </div>
                <div class="homepage-right col-lg-6">
                    <h2>A TUDÁS, HATALOM!</h2>
                    <p>A második B osztály büszke arra a szemléletmódra, mely a tanulás mellett az elfogadásra és az emberi léptékre is koncentrál.</p>
                    <p>Ezen szemlélet kialakításában és átadásában tanárainknak elévülhetetlen érdemei vannak! Köszönjük Nekik!</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>




    <!-- articles -->
    <div class="service-section">
        <div class="col-md-7 service-section-grids">
            <div class="container">
                <div class="serve-head">
                    <h3>{{$article->title}}</h3>
                    <h6>@lang('page.written_by'): Tamas Dogi | @lang('page.created_at'): {{$article->created_at}} </h6>
                </div>
            </div>
            <div class="service-grid">
                <div class="service-section-grid">
                    <div class="icon">
                        <i class="book"> </i>
                    </div>
                    <div class="icon-text">
                        <p>{!! $article->description !!}</p>
                    </div>
                    <span class="views">@lang('page.views'): {{$article->views}} | </span>
                    <span class="likes">@lang('page.likes'): <span id="like">{{$article->likes}} </span></span>
                    <button class="btn btn-like" onclick="incrementLikes({{$article->id}})">LIKE</button>

                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-5 service-text">
            <p></p>
        </div>
        <div class="clearfix"></div>
    </div>
    <!--- 728x90 --->
    <script>

        function incrementLikes(id) {

            $.ajax({url: "/increase-likes/" + id, success: function(result){
                    var likes = $("#like").html();
                    $("#like").html(parseInt(likes) + 1);
                }});
        }

    </script>
    <!-- /services -->


    <div class="mid-bg">
        <div class="container">
            <div class="mid-section">
                <h3>Első nap a suliban!</h3>
                <h4>Készen állsz?</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy text and
                    typesetting industry. Lorem Ipsum has been the industry's standard dummy
                    ever since the 1500s,</p>
            </div>
        </div>
    </div>
    <!--address-->
    <div id="contact" class="address">
        <div class="col-md-7 address-left">
            <div class="products">
                <h3>Órarend</h3>
                <ul>
                    <li><a href="#">Testnevelés</a></li>
                    <li><a href="#">Környezet</a></li>
                    <li><a href="#">Matematika</a></li>
                </ul>
            </div>
            <div class="company-adout">
                <h3>Órarend</h3>
                <ul>
                    <li><a href="">About</a></li>
                    <li><a href="">Teacher</a></li>
                    <li><a href="">Contact</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="col-md-5 address-right">
            <h3>Elérhetőség</h3>
            <p>Budapest, Ürömi út</p>
            <p>04 84 25 51 54</p>
            <ul class="bottom">
                <li>Email: info@vackor.hu</li>
                <li>Web: www.vackor.hu</li>
            </ul>

        </div>
        <div class="clearfix"></div>
    </div>































@endsection
