@extends('layouts.app')

@section('content')

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />

    <div class="container">

      <div class="col-lg-2">
          @include('shared.latest')
      </div>

      <div class="col-lg-8">

          <div class="content">

              <div class="panel panel-default">
                  <div class="panel-heading">
                      <h1>Eseménynaptár</h1>
                  </div>

                  <div class="panel-body">
                      <div id='calendar'></div>
                  </div>
              </div>


          </div>

      </div>

      <div class="col-lg-2">
          @include('shared.popular')
      </div>

    </div>
    <script>
        $(document).ready(function() {
            // page is now ready, initialize the calendar...
            $('#calendar').fullCalendar({
                // put your options and callbacks here
                locale: 'hu',
                events : [
                  @foreach($tasks as $task)
                  {
                      title : '{{ $task->name }}',
                      start : '{{ $task->task_date }}',
                      url : 'tasks/show/{{ $task->id }}'
                  },
                  @endforeach
                ]
            })
        });
    </script>


@endsection
