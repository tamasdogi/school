@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Articles in category</h2>
                    </div>

                    <div class="panel-body">
                        @foreach($articles as $item)
                            <h3><a href="/article/{{$item->id}}">{{$item->title}}</a></h3>
                            <span>{!! $item->desc !!}</span>
                        @endforeach
                        @if(count($articles) < 1)
                            There is no posts available yet.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
