@if(!Auth::User())
<div class="side-menu">
    <h2>Menü</h2>
    <ul>
      <li><a href="/events">Eseménynaptár</a></li>
      <li><a href="#">Üzenő</a></li>
      <li><a href="#">Levélküldés</a></li>
      <li><a href="#">Képtár</a></li>
    </ul>
</div>

<div class="side-menu">
    <h2>Linkek</h2>
    <ul>
      <li><a href="http://ujlaki.ultranet.hu/" target="_blank">Iskolánk honlapja</a></li>
      <li><a href="http://findusz.gportal.hu/">6. C honlapja</a></li>
    </ul>
</div>

<div class="side-menu">
    <h2>Biztonságos internet
</h2>
    <ul>
      <li><a href="http://saferinternet.hu/tippek-videok/gyerekeknek" target="_blank">Szabályok barikás videókkal</a></li>
      <li><a href="http://saferinternet.hu/">Tanácsok</a></li>
      <li><a href="http://www.matisz.hu/fileadmin/template/dokumentumok/matisz/dokumentumok/EU.Kids.Online.Safety.Tips-HU.pdf">Szabályok</a></li>
    </ul>
</div>

<div class="side-menu">
    <h2>Oktatóprogramok, hasznos linkek	</h2>
    <ul>
      <li><a href="http://www.okosdoboz.hu/">Okosdoboz</a></li>
      <li><a href="http://gyereketeto.hu/">Gyereketető</a></li>
      <li><a href="http://szorzotabla.hu/">Szorzótábla gyakorlása</a></li>
      <li><a href="http://learningapps.org/">Tankockák</a></li>
      <li><a href="http://www.gyakorolj.hu/oktato/oktato/elsomagyar.php">Gyakorolj!</a></li>
      <li><a href="http://www.aweben.hu/matek/szorzotabla.htm?">Szorzótáblák gyakorlása</a></li>
    </ul>
</div>

<div class="side-menu">
    <h2>Latest elements</h2>
    <ul>
    @foreach(App\Article::orderBy('title', 'desc')->take(10)->get() as $item)
            <li><a href="/article/{{$item->id}}">{{$item->title}}</a></li>
    @endforeach
    </ul>
</div>
@endif

@if(Auth::User() && Auth::User()->id === 1)
Súgó az admin kezeléséhez.
@endif
