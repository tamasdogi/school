@if(Auth::User() && Auth::User()->id === 1)
  <div class="side-menu admin-menu">
      <h2>@lang('admin.administration')</h2>
      <ul>
            <div class="hr">Bejegyzések</div>
            <li><a href="{{ route('blog-entries') }}">@lang('admin.blog_list')</a></li>

            <div class="hr">@lang('admin.categories')</div>
            <li><a href="{{ route('category-entries') }}">@lang('admin.category_list')</a></li>

            <div class="hr">@lang('admin.events')</div>
            <li><a href="/tasks">@lang('admin.event_list')</a></li>

            <div class="hr">@lang('admin.galleries')</div>
            <li><a href="{{ route('galleries') }}">@lang('admin.galleries')</a></li>

          <div class="hr">Tanárok kezelés</div>
          <li><a href="{{ route('teachers') }}">Tanárok listája</a></li>

          <div class="hr">Tanulók kezelés</div>
          <li><a href="{{ route('students') }}">Tanulók listája</a></li>

          <div class="hr">Szülők kezelés</div>
          <li><a href="{{ route('parents') }}">Szülők listája</a></li>

          <div class="hr">Órarend kezelés</div>
          <li><a href="/lessons/a">A hét </a></li>
          <li><a href="/lessons/b">B hét </a></li>

          <div class="hr">@lang('admin.account')</div>
            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    @lang('admin.logout')
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
  </div>
@endif

@if(!Auth::User())
<div class="side-menu">
    <h2>@lang('admin.most_viewed')</h2>
    <ul>
        @foreach(App\Article::orderBy('views', 'desc')->take(5)->get() as $item)
            <li><a href="/article/{{$item->id}}">{{$item->title}}</a></li>
        @endforeach
    </ul>
</div>

<div class="side-menu">
    <h2>@lang('admin.gallery')</h2>
    <ul>
        @foreach(App\Gallery::orderBy('created_at', 'desc')->take(15)->get() as $item)
            <li><a href="/gallery/album/{{$item->id}}">{{$item->title}}</a></li>
        @endforeach
    </ul>
</div>
@endif
