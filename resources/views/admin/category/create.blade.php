@extends('layouts/app')

@section('content')

    <div class="container">

        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">

            <div class="card uper">
                <h3 class="card-header">
                    Create new category
                </h3>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br/>
                    @endif

                    @if ($success && $success['status'] === true)
                        <div class="alert alert-success"> {{$success['msg']}}</div>
                    @endif

                    <form method="post" action="/store-category">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Category name:</label>
                            <input type="text" class="form-control" name="name"/>
                        </div>
                        <div class="form-group">
                            <label for="price">Description :</label>
                            <input type="text" class="form-control" name="desc"/>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Slug:</label>
                            <input type="text" class="form-control" name="slug"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
