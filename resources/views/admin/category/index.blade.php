@extends('layouts/app')

@section('content')

    <div class="container">

        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">

            <div class="card uper">
                <h3 class="card-header">
                    Lista oldal categories
                </h3>
                <div class="card-body">


                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">@lang('admin.title')</th>
                            <th scope="col">Slug</th>
                            <th scope="col" style="width: 5%; text-align: center;">@lang('admin.edit')</th>
                            <th scope="col" style="width: 5%; text-align: center;">@lang('admin.delete')</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($categories as $item)
                            <tr>
                                <th scope="row">{{ $item->id }}</th>
                                <th>{{ $item->title }}</th>
                                <th>{{ $item->slug }}</th>
                                <th style="text-align: center;"><a href="/category-edit/{{ $item->id }}"><i class='glyphicon glyphicon-edit' style='font-size:20px'></i></a></th>
                                <th style="text-align: center;"><a href="/category-delete/{{ $item->id }}"><i class='glyphicon glyphicon-trash' style='font-size:20px'></i></a></th>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    <a href="{{ route('create-new-category') }}"><button class="btn btn-primary">Új kategória</button></a>
                </div>
            </div>
        </div>



    </div>
@endsection
