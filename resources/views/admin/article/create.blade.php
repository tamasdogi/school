@extends('layouts/app')

@section('content')

    <script>
        $(document).ready(function () {
            $('#summernote').summernote();
        });
    </script>

    <div class="container">
        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">
            <div class="card uper">
                <h3 class="card-header">
                    Create new blog entry
                </h3>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br/>
                    @endif

                    @if ($success && $success['status'] === true)
                        <div class="alert alert-success"> {{$success['msg']}}</div>
                    @endif

                    <form method="post" action="/store-article" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Post title:</label>
                            <input type="text" class="form-control" name="title"/>
                        </div>
                        <div class="form-group">
                            <label for="price">Description:</label>
                            <textarea id="summernote" class="form-control" name="desc" rows="20"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Slug:</label>
                            <input type="text" class="form-control" name="slug"/>
                        </div>

                        <div class="form-group">
                            <label for="quantity">Image:</label>
                            <input type="file" class="form-control" name="image"/>
                        </div>

                        <div class="form-group">
                            <label for="quantity">Category:</label>
                            <select class="form-control" name="category_id">
                                @foreach ($cat->all() as $item)
                                    <option value="{{$item->id}}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Tags:</label>
                            <input type="text" class="form-control" name="tag"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
