@extends('layouts/app')

@section('content')

    <div class="container">

        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">

            <div class="card uper">
                <h3 class="card-header">
                    Lista oldal blog
                </h3>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Slug</th>
                            <th scope="col">Category</th>
                            <th scope="col" style="width: 5%; text-align: center;">@lang('admin.edit')</th>
                            <th scope="col" style="width: 5%; text-align: center;">@lang('admin.delete')</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($articles as $item)
                            <tr>
                                <th scope="row">{{ $item->id }}</th>
                                <th>{{ $item->title }}</th>
                                <th>{{ $item->slug }}</th>
                                <th>{{ $item->category_title }}</th>
                                <th style="text-align: center;"><a href="/article-edit/{{ $item->id }}"><i class='glyphicon glyphicon-edit' style='font-size:20px'></i></a></th>
                                <th style="text-align: center;"><a href="/article-delete/{{ $item->id }}"><i class='glyphicon glyphicon-trash' style='font-size:20px'></i></a></th>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <a href="{{ route('create-new-entry') }}"><button class="btn btn-primary">@lang('admin.create_new_blog')</button></a>


                </div>
            </div>
        </div>

    </div>
@endsection
