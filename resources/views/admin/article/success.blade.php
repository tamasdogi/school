@extends('layouts/app')

@section('content')


    <div class="container">

        <div class="col-lg-2">
            @include('shared.latest')
        </div>
        <div class="col-lg-8">
            <div class="card uper">
                <h3>
                    Edit category
                </h3>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br/>
                    @endif

                    @if ($success && $success['status'] === true)
                        <div class="alert alert-success"> {{$success['msg']}}</div>
                    @endif

                </div>
            </div>
            <div class="col-lg-2">
                @include('shared.latest')
            </div>
        </div>
    </div>

@endsection
