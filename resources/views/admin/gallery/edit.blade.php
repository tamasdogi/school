@extends('layouts/app')

@section('content')

    <div class="container">


        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">
            <h3 class="card-header">
                @lang('admin.edit_gallery')
            </h3>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif

                @if ($success && $success['status'] === true)
                    <div class="alert alert-success"> {{$success['msg']}}</div>
                @endif

                <form method="post" action="/update-gallery/{{$gallery->id}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">@lang('admin.title')</label>
                        <input value="{{$gallery->title}}" type="text" class="form-control" name="title"/>
                    </div>
                    <div class="form-group">
                        <label for="price">@lang('admin.description'):</label>
                        <input value="{{$gallery->description}}" type="text" class="form-control" name="description"/>
                    </div>

                    <button type="submit" class="btn btn-primary">@lang('admin.update')</button>
                </form>

                <h3>Képek hozzáadása</h3>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form action="/uploadfile" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <input type="file" name="image[]" id="image" multiple>
                        <input type="hidden" name="gallery_id" value="{{$gallery->id}}">
                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">@lang('admin.create')</button>
                </form>

                <h4>Feltöltött képek:</h4>
                <div class="row">
                    @foreach($pics as $pic)
                        <div class="col-lg-3 image-row">
                            <img width="100%" height="60" src="/storage/images/thumbnail/{{$pic->title}}">
                            <a href="/delete-image/{{$gallery->id}}/{{$pic->id}}"> <i class="trash"></i></a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>




    </div>
@endsection
