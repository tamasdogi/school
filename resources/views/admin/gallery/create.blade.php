@extends('layouts/app')

@section('content')

    <div class="container">


        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">
            <h3 class="card-header">
                @lang('admin.new_gallery')
            </h3>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif

                @if ($success && $success['status'] === true)
                   <div class="alert alert-success"> {{$success['msg']}}</div>
                @endif

                <form method="post" action="/store-gallery">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">@lang('admin.title'):</label>
                        <input type="text" class="form-control" name="title"/>
                    </div>
                    <div class="form-group">
                        <label for="price">@lang('admin.description'):</label>
                        <input type="text" class="form-control" name="description"/>
                    </div>

                    <button type="submit" class="btn btn-primary">@lang('admin.create')</button>
                </form>

            </div>
        </div>




    </div>
@endsection
