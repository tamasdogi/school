@extends('layouts/app')

@section('content')

    <div class="container">

        <div class="col-lg-2">
            @include('shared.popular')
        </div>

      <div class="col-lg-10">

        <div class="card uper">
            <h3 class="card-header">
                @lang('admin.gallery_manager')
            </h3>
            <div class="card-body">
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">@lang('admin.title')</th>
                        <th scope="col" style="width: 5%; text-align: center;">@lang('admin.edit')</th>
                        <th scope="col" style="width: 5%; text-align: center;">@lang('admin.delete')</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach ($galleries as $item)
                          <tr>
                              <th scope="row"  width="5%">{{ $item->id }}</th>
                              <th>{{ $item->title }} ({{ App\Http\Controllers\Admin\GalleryController::CountPics($item->id) }})</th>
                              <th style="text-align: center;"><a href="/gallery-edit/{{ $item->id }}"><i class='glyphicon glyphicon-edit' style='font-size:20px'></i></a></th>
                              <th style="text-align: center;"><a href="/gallery-delete/{{ $item->id }}"><i class='glyphicon glyphicon-trash' style='font-size:20px'></i></a></th>
                          </tr>
                      @endforeach
                    </tbody>
                </table>


                <a href="{{ route('create-gallery') }}"><button class="btn btn-primary">@lang('admin.new_gallery')</button></a>

            </div>
        </div>
      </div>




    </div>
@endsection
