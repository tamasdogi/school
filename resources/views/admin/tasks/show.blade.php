@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Esemény adatai</h2>
                    </div>
                    <div class="panel-body">
                        <div>Esemény neve: {{$event->name}}</div>
                        <div>Időpontja: {{$event->task_date}}</div>
                        <div>Leírás:<br>{{$event->description}}</div>
                    </div>
                </div>
        </div>

    </div>

@endsection
