@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">
            <h3>Esemény módosítása</h3>
            <form action="/tasks/{{$event->id}}/update" method="POST">
                {{ csrf_field() }}
                Esemény neve:
                <br/>
                <input class="form-control" type="text" name="name" value="{{$event->name}}" />
                <br/><br/>
                Esemény leírása:
                <br/>
                <textarea name="description" class="form-control">{{$event->description}}</textarea>
                <br/><br/>
                Esemény időpontja:
                <br/>
                <input type="date" name="task_date" class="date form-control" value="{{$event->task_date}}" />
                <br/><br/>
                <input class="btn btn-primary" type="submit" value="Mentés"/>
            </form>
        </div>

    </div>
@endsection
