@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-lg-2">
            @include('shared.popular')
        </div>

        <div class="col-lg-10">
            <link rel='stylesheet'
                  href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css'/>

            <h3>Események kezelése</h3>

            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">@lang('admin.title')</th>
                    <th scope="col">Date</th>
                    <th scope="col">Created at</th>
                    <th scope="col" style="width: 5%; text-align: center;">@lang('admin.edit')</th>
                    <th scope="col" style="width: 5%; text-align: center;">@lang('admin.delete')</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($events as $item)

                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <th>{{ $item->name }}</th>
                        <th>{{ $item->task_date }}</th>
                        <th>{{ $item->created_at }}</th>
                        <th style="text-align: center;"><a href="/tasks/{{ $item->id }}/edit"><i class='glyphicon glyphicon-edit' style='font-size:20px'></i></a></th>
                        <th style="text-align: center;"><a href="/delete-task/{{ $item->id }}"><i class='glyphicon glyphicon-trash' style='font-size:20px'></i></a></th>
                    </tr>
                @endforeach

                </tbody>
            </table>

            <a href="/tasks/create"><button class="btn btn-primary">@lang('admin.create_new_event')</button></a>

        </div>

    </div>
@endsection
