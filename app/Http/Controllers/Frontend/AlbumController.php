<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pics;

class AlbumController extends Controller
{

       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $pics = Pics::where('gallery_id', $id)->orderBy('title', 'desc')->get();
        $album = Gallery::where('id', $id)->orderBy('title', 'desc')->value('title');

        return view('frontend.gallery.album', ['pics' => $pics, 'album' => $album]);
    }


}
