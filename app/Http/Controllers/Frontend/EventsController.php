<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\Category;
use App\Article;
use App\Task;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tasks = Task::all();

        $latest = Article::orderBy('title', 'desc')
            ->take(10)
            ->get();

        $category = Category::orderBy('title', 'desc')
            ->take(10)
            ->get();

        return view('frontend.events.events', ['categories' => $category, 'latest' => $latest, 'tasks' => $tasks]);
    }

}
