<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Article;
use App\Task;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tasks = Task::all();

        $latest = Article::orderBy('title', 'desc')
            ->take(10)
            ->get();

        $category = Category::orderBy('title', 'desc')
            ->take(10)
            ->get();

        return view('frontend.welcome', ['categories' => $category, 'articles' => $latest, 'tasks' => $tasks]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function category($id)
    {
        $articles = Article::where('category_id', $id)->orderBy('title', 'desc')
            ->take(10)
            ->get();

        return view('frontend.category', ['articles' => $articles]);
    }

    public function article($id)
    {

        $latest = Article::orderBy('title', 'desc')
            ->take(10)
            ->get();

        DB::table('articles')->where('id', $id)->increment('views');
        $article = Article::where('id', $id)->first();

        return view('frontend.article', ['article' => $article, 'latest' => $latest]);
    }

    public function homepage()
    {
        $articles = Article::where('category_id', '3')->get();

        return view('frontend.welcome', ['articles' => $articles]);
    }

    public function likeIt($id)
    {
        DB::table('articles')->where('id', $id)->increment('likes');
    }


}
