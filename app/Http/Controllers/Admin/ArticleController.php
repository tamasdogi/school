<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Article;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Image; //Intervention Image
use Illuminate\Support\Facades\Storage; //Laravel Filesystem


class ArticleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('title', 'desc')
            ->leftjoin('categories','categories.id','=','articles.category_id')
            ->select('categories.title as category_title', 'articles.title', 'articles.slug', 'articles.id')
            ->take(100)
            ->get();

        return view('admin.article.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $success = [];
        $cat = $this->getCategories();
        return view('admin.article.create', ['success' => $success, 'cat' => $cat]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = [];

        $data['name'] = $request->input('title');
        $data['slug'] = $request->input('slug');
        $data['desc'] = $request->input('desc');
        $pics = $this->uploadPics($request);

        $article = new Article;
        $article->title = $data['name'];
        $article->user_id = Auth::id();
        $article->category_id = $request->input('category_id');
        $article->slug = $data['slug'];
        $article->description = $data['desc'];
        $article->pics = $pics;
        $article->status = 0;
        $article->views = 0;
        $article->likes = 0;
        $article->tags =  $request->input('tag');
        $article->save();

        $cat = $this->getCategories();
        $success['status'] = true;
        $success['msg'] = 'Article has been created!';
        return view('admin.article.create', ['data' => $data, 'success' => $success, 'cat' => $cat]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $success = [];
        $category = Article::where('id', $id)->orderBy('title', 'desc')
            ->take(1)
            ->first();
        return view('admin.article.show', ['categories' => $category, 'success' => $success]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $success = [];
        $article = Article::where('id', $id)->orderBy('title', 'desc')
            ->take(1)
            ->first();
        $cat = $this->getCategories();
        return view('admin.article.edit', ['article' => $article, 'success' => $success, 'id' => $id, 'cat' => $cat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = [];
        DB::table('articles')
            ->where('id', $id)
            ->update(['category_id' => $request->input('category_id'), 'title' => $request->input('title'), 'slug' => $request->input('slug'), 'description' => $request->input('desc'), 'tags' => $request->input('tags')]);

        $cat = $this->getCategories();
        $success['status'] = true;
        $success['msg'] = 'Category has been updated succesfully!';
        return view('admin.article.success', ['success' => $success, 'cat' => $cat]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return view('admin.index');
    }

    private function getCategories() {
        $articles = Category::orderBy('title', 'desc')->get();
        return $articles;
    }

    public function uploadPics($request)
    {

                $file = $request->file('image');

                $this->validate($request, [
                    'image' => 'required',
                    'image.*' => 'mimes:jpeg,png,gif'
                ]);

                $filesize = $file->getSize();
                //get filename with extension
                $filenamewithextension = $file->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $file->getClientOriginalExtension();

                //filename to store
                $filenametostore = $filename . '_' . uniqid() . '.' . $extension;

                /*Storage::put('public/profile_images/'. $filenametostore, fopen($file, 'r+'));*/
                Storage::put('public/images/articles/thumbnail/' . $filenametostore, fopen($file, 'r+'));

                //Resize image here
                $thumbnailpath = public_path('storage/images/articles/thumbnail/' . $filenametostore);
                $img = Image::make($thumbnailpath)->resize(800, 500, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($thumbnailpath);

                return $filenametostore;
            }



}
