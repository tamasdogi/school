<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Image; //Intervention Image
use Illuminate\Support\Facades\Storage; //Laravel Filesystem

use Illuminate\Support\Facades\File;

use Illuminate\Http\Request;

use App\Pics;

class UploadFileController extends Controller {

    public function index() {
        return view('admin.uploadfile');
    }

    public function store(Request $request) {


        if ($request->hasFile('image')) {

            foreach($request->file('image') as $file){

                $this->validate($request, [
                    'image' => 'required',
                    'image.*' => 'mimes:jpeg,png,gif'
                ]);

                echo $filesize = $file->getSize();
                //get filename with extension
                $filenamewithextension = $file->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $file->getClientOriginalExtension();

                //filename to store
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;

                /*Storage::put('public/profile_images/'. $filenametostore, fopen($file, 'r+'));*/
                Storage::put('public/images/thumbnail/'. $filenametostore, fopen($file, 'r+'));

                //Resize image here
                $thumbnailpath = public_path('storage/images/thumbnail/'.$filenametostore);
                $img = Image::make($thumbnailpath)->resize(800, 500, function($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($thumbnailpath);

                $pics = new Pics;
                $pics->gallery_id = $request->gallery_id;
                $pics->title = $filenametostore;
                $pics->description = 'description';
                $pics->status = 1;
                $pics->view = 0;
                $pics->like = 0;
                $pics->save();
            }

            return redirect('gallery-edit/'.$request->gallery_id)->with('status', "A fájl feltöltése sikeres volt.");
        }

    }

    public function delete($gallery_id, $pic_id) {

            $pics = Pics::where('id', $pic_id)->first();
            $pics->delete();

            Storage::delete($pics->title);
            unlink('storage/images/thumbnail/'.$pics->title);

        return redirect('gallery-edit/'.$gallery_id)->with('status', "A fájl törlése sikeres volt.");

    }


}
