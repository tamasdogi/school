<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::orderBy('title', 'desc')
            ->take(10)
            ->get();

        return view('admin.category.index', ['categories' => $category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $success = [];
        return view('admin.category.create', ['success' => $success]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = [];

        $data['name'] = $request->input('name');
        $data['slug'] = $request->input('slug');
        $data['desc'] = $request->input('desc');

        $category = new Category;
        $category->title = $data['name'];
        $category->slug = $data['slug'];
        $category->desc = $data['desc'];
        $category->save();

        $success['status'] = true;
        $success['msg'] = 'Category has been created!';
        return view('admin.category.create', ['data' => $data, 'success' => $success]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $success = [];
        $category = Category::where('id', $id)->orderBy('title', 'desc')
            ->take(1)
            ->first();
        return view('admin.category.show', ['categories' => $category, 'success' => $success]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $success = [];
        $category = Category::where('id', $id)->orderBy('title', 'desc')
            ->take(1)
            ->first();
        return view('admin.category.edit', ['category' => $category, 'success' => $success]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = [];

        DB::table('categories')
            ->where('id', $id)
            ->update(['title' => $request->input('name'), 'slug' => $request->input('slug'), 'desc' => $request->input('desc')]);

        $success['status'] = true;
        $success['msg'] = 'Category has been updated succesfully!';
        return view('admin.category.success', ['success' => $success]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return view('admin.category.index');
    }
}
