<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Pics;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PicsController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $pics = Pics::orderBy('title', 'desc')
          ->leftjoin('gallery','gallery.id','=','pics.gallery_id')
          ->select('gallery.title as gallery_title', 'pics.title', 'pics.id')
          ->take(100)
          ->get();

      return view('admin.pics.index', ['pics' => $pics]);
  }


}
