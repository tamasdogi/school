<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pics;

class GalleryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::orderBy('title', 'desc')->get();

        return view('admin.gallery.index', ['galleries' => $galleries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $success = [];
        return view('admin.gallery.create', ['success' => $success]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = [];

        $data['title'] = $request->input('title');
        $data['description'] = $request->input('description');

        $gallery = new Gallery;
        $gallery->title = $request->input('title');
        $gallery->description = $request->input('description');
        $gallery->status = 1;
        $gallery->view = 0;
        $gallery->like = 0;
        $gallery->save();

        $success['status'] = true;
        $success['msg'] = 'Gallery has been created!';
        return view('admin.gallery.create', ['data' => $data, 'success' => $success]);
    }

    public function edit($id)
    {
        $success = [];
        $gallery = Gallery::where('id', $id)->first();

        $pics = Pics::where('gallery_id', $id)->get();

        return view('admin.gallery.edit', ['success' => $success, 'gallery' => $gallery, 'pics' => $pics]);
    }

    public function update(Request $request, $id)
    {
        $success = [];
        DB::table('gallery')
            ->where('id', $id)
            ->update(['title' => $request->input('title'), 'description' => $request->input('description')]);
        $success['status'] = true;
        $success['msg'] = 'Gallery has been updated succesfully!';

        $gallery = Gallery::where('id', $id)->first();

        return view('admin.gallery.edit', ['success' => $success, 'gallery' => $gallery]);

    }

    public function delete($id)
    {
        $success = [];
        DB::table('gallery')->where('id', $id)->delete();

        $galleries = Gallery::orderBy('title', 'desc')->get();
        return view('admin.gallery.index', ['galleries' => $galleries]);
    }

    public static function countPics($id) {
        return $pics = Pics::where('gallery_id', $id)->count();
    }



}
