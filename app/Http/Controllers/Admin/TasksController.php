<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{

  public function index()
  {
      $events = Task::all();
      return view('admin.tasks.index', ['events' => $events]);
  }

  public function create()
  {
      return view('admin.tasks.create');
  }

  public function store(Request $request)
  {
      Task::create($request->all());
      return redirect()->route('taskSuccess')->with('Sikeres létrehozás!');
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Task::where('id', $id)->first();
        return view('admin.tasks.show', ['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Task::where('id', $id)->first();
        return view('admin.tasks.edit', ['event' => $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Task::where('id', $id)->first();
        $event->update(['name' => $request->input('name'), 'description' => $request->input('description'), 'task_date' => $request->input('task_date')]);

        return redirect()->route('taskSuccess');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Task::where('id', $id)->first();
        $event->delete();

        return redirect()->route('taskSuccess');
    }
}
