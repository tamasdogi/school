<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pics extends Model
{
  protected $fillable = ['gallery_id', 'title', 'description', 'task_date', 'status', 'view', 'like'];
}
