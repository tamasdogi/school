<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
      protected $table = 'gallery';
      protected $fillable = ['title', 'description', 'task_date', 'status', 'view', 'like'];
}
